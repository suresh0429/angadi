package com.innasoft.kilomart.Interface;

import com.innasoft.kilomart.Response.CheckoutResponse;

import java.util.List;

public interface PaymentTypeInterface {

    void onItemClick(List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position);
}
