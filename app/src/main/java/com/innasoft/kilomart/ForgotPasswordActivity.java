package com.innasoft.kilomart;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Singleton.AppController;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    String OTP;
    boolean isConnected;

    @BindView(R.id.etPhone)
    TextInputEditText etPhone;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etOtp)
    TextInputEditText etOtp;
    @BindView(R.id.txtIpOtpLayout)
    TextInputLayout txtIpOtpLayout;
    @BindView(R.id.txtResendOtp)
    TextView txtResendOtp;
    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.txtIpNameLayout)
    TextInputLayout txtIpNameLayout;
    @BindView(R.id.etsetPassword)
    TextInputEditText etsetPassword;
    @BindView(R.id.txtIpPwdLayout)
    TextInputLayout txtIpPwdLayout;
    @BindView(R.id.secondLayout)
    LinearLayout secondLayout;
    @BindView(R.id.btnContinue)
    Button btnContinue;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    String deviceID;
    AppController app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forgot Password");
        app = (AppController) getApplication();

        deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);


        etPhone.addTextChangedListener(new MyTextWatcher(etPhone));
        etOtp.addTextChangedListener(new MyTextWatcher(etOtp));
        etsetPassword.addTextChangedListener(new MyTextWatcher(etsetPassword));
    }


    @OnClick({R.id.txtResendOtp, R.id.btnContinue})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtResendOtp:
                // Method to manually check connection status
                isConnected = app.isConnection();

                if (isConnected) {
                    sendOtp();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    snackBar(message, color);
                    //showSnack(isConnected);
                }


                break;
            case R.id.btnContinue:



                // Method to manually check connection status
                isConnected = app.isConnection();

                if (isConnected) {

                    if (btnContinue.getText().toString().trim().equalsIgnoreCase("Submit")) {
                        passwordChange();
                    } else {
                        sendOtp();
                    }


                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);

                }

                try  {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }

                break;
        }
    }


    // send OTP
    private void sendOtp() {

        String mobile = etPhone.getText().toString().trim();
        if ((!isValidPhoneNumber(mobile))) {
            return;
        }


        progressBar.setVisibility(View.VISIBLE);
        btnContinue.setEnabled(false);

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userForgotPassword(mobile);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);

                BaseResponse baseResponse = response.body();


                if (baseResponse.getStatus().equalsIgnoreCase("10100")) {

                    secondLayout.setVisibility(View.VISIBLE);
                    // etName.setText(baseResponse.getUsername());
                    btnContinue.setText("Submit");

                    // OTP = baseResponse.get();

                    int color = Color.GREEN;
                    snackBar(baseResponse.getMessage(), color);

                }else if (baseResponse.getStatus().equals("10200")){
                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (baseResponse.getStatus().equals("10300")){
                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (baseResponse.getStatus().equals("10400")){
                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    int color = Color.RED;
                    snackBar(baseResponse.getMessage(), color);

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);
                btnContinue.setText("Continue");
            }
        });
    }

    // password change
    private void passwordChange() {

        String mobile = etPhone.getText().toString().trim();
        String otp = etOtp.getText().toString().trim();
        String password = etsetPassword.getText().toString().trim();

        if ((!isValidPhoneNumber(mobile))) {
            return;
        }

       /* if ((!isValidOtp(otp))) {
            return;
        }*/

        if ((!isValidatePassword(password))) {
            return;
        }


        progressBar.setVisibility(View.VISIBLE);
        btnContinue.setEnabled(false);

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userVerifyForgotPassword(mobile,otp,deviceID,password);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);

                BaseResponse baseResponse = response.body();


                if (baseResponse.getStatus().equalsIgnoreCase("10100")) {

                    int color = Color.GREEN;
                    snackBar(baseResponse.getMessage(), color);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                        }
                    }, 2000);


                }else if (baseResponse.getStatus().equals("10200")){
                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (baseResponse.getStatus().equals("10300")){
                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (baseResponse.getStatus().equals("10400")){
                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    int color = Color.RED;
                    snackBar(baseResponse.getMessage(), color);

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnContinue.setEnabled(true);

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }


    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhone);
            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhone);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    // valid OTP
    private boolean isValidOtp(String otp) {

        if (otp.isEmpty()) {
            txtIpOtpLayout.setError("OTP is required");
            requestFocus(etOtp);
            return false;
        } else if (!otp.equalsIgnoreCase(OTP) || OTP.length() < 4) {
            txtIpOtpLayout.setError("Enter a valid OTP");
            requestFocus(etOtp);
            return false;
        } else {
            txtIpOtpLayout.setErrorEnabled(false);
        }

        return true;
    }


    // validate password
    private boolean isValidatePassword(String password) {
        if (password.isEmpty()) {
            txtIpPwdLayout.setError("Password required");
            requestFocus(etsetPassword);
            return false;
        } else if (password.length() < 6 || password.length() > 20) {
            txtIpPwdLayout.setError("Password Should be 6 to 20 characters");
            requestFocus(etsetPassword);
            return false;
        } else {
            txtIpPwdLayout.setErrorEnabled(false);
        }

        return true;
    }


    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.etPhone:
                    isValidPhoneNumber(etPhone.getText().toString().trim());
                    break;
               /* case R.id.etOtp:
                    isValidOtp(etOtp.getText().toString().trim());
                    break;*/
                case R.id.etsetPassword:
                    isValidatePassword(etsetPassword.getText().toString().trim());
                    break;
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
