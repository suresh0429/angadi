package com.innasoft.kilomart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.innasoft.kilomart.Adapters.NotificationsAdapter;
import com.innasoft.kilomart.Adapters.PaymentTpyeRecyclerAdapter;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Response.NotificationsResponse;
import com.innasoft.kilomart.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {
    @BindView(R.id.notifction_recycleview)
    RecyclerView notifctionRecycleview;
    String userId,email, tokenValue, deviceId;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PrefManager pref;
    NotificationsAdapter notificationsAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notifications");



        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        email = profile.get("email");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        Call<NotificationsResponse> call = RetrofitClient.getInstance().getApi().Notifications(tokenValue, userId);
        call.enqueue(new Callback<NotificationsResponse>() {
            @Override
            public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                if (response.isSuccessful());
                NotificationsResponse notificationsResponse=response.body();
                if (notificationsResponse.getStatus().equals("10100")){
                    progressBar.setVisibility(View.GONE);
//                    Toast.makeText(NotificationActivity.this, notificationsResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    List<NotificationsResponse.DataBean> dataBeans=notificationsResponse.getData();

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    notifctionRecycleview.setLayoutManager(layoutManager);
                    notifctionRecycleview.setItemAnimator(new DefaultItemAnimator());
                    notificationsAdapter = new NotificationsAdapter(NotificationActivity.this, dataBeans);
                    notifctionRecycleview.setAdapter(notificationsAdapter);
                }
                else if (notificationsResponse.getStatus().equals("10200")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(NotificationActivity.this, notificationsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (notificationsResponse.getStatus().equals("10300")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(NotificationActivity.this, notificationsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (notificationsResponse.getStatus().equals("10400")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(NotificationActivity.this, notificationsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(NotificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
