package com.innasoft.kilomart.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : [{"id":"1","notification_id":"1","title":"cxvxcv","message":"cvzxcv","created_on":"0000-00-00 00:00:00","is_read":"122","image":""}]
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * notification_id : 1
         * title : cxvxcv
         * message : cvzxcv
         * created_on : 0000-00-00 00:00:00
         * is_read : 122
         * image :
         */

        @SerializedName("id")
        private String id;
        @SerializedName("notification_id")
        private String notificationId;
        @SerializedName("title")
        private String title;
        @SerializedName("message")
        private String message;
        @SerializedName("created_on")
        private String createdOn;
        @SerializedName("is_read")
        private String isRead;
        @SerializedName("image")
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNotificationId() {
            return notificationId;
        }

        public void setNotificationId(String notificationId) {
            this.notificationId = notificationId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getIsRead() {
            return isRead;
        }

        public void setIsRead(String isRead) {
            this.isRead = isRead;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
