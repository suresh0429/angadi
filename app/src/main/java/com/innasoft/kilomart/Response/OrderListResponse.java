package com.innasoft.kilomart.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderListResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"total_products":2,"order_details":{"reference_id":"KM2019091604072927","order_confirmation_reference_id":"KM2019091604072998","total_mrp_price":"450","discount":"0","shipping_charges":"0","final_price":"450","cancel_charges":"0","refund_price":"0","pincode":"500081","medium":"ANDROID","order_status":"Pending","status":"0","getaway_name":"CcAvenue","ip_address":"157.48.97.171","expected_delivery":"N/A","expected_delivery_slot":"N/A","order_date":"16-09-2019 04:07 pm"},"status":[],"products":[{"id":"5","product_id":"5","product_name":"Peanuts","images":"3542a-peanuts.png","product_mrp_price":"60","purchase_quantity":"5","total_price":"300","status":"0"},{"id":"16","product_id":"4","product_name":"Chana Dal","images":"64121-chana-dal.png","product_mrp_price":"100","purchase_quantity":"5","total_price":"450","status":"0"}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * total_products : 2
         * order_details : {"reference_id":"KM2019091604072927","order_confirmation_reference_id":"KM2019091604072998","total_mrp_price":"450","discount":"0","shipping_charges":"0","final_price":"450","cancel_charges":"0","refund_price":"0","pincode":"500081","medium":"ANDROID","order_status":"Pending","status":"0","getaway_name":"CcAvenue","ip_address":"157.48.97.171","expected_delivery":"N/A","expected_delivery_slot":"N/A","order_date":"16-09-2019 04:07 pm"}
         * status : []
         * products : [{"id":"5","product_id":"5","product_name":"Peanuts","images":"3542a-peanuts.png","product_mrp_price":"60","purchase_quantity":"5","total_price":"300","status":"0"},{"id":"16","product_id":"4","product_name":"Chana Dal","images":"64121-chana-dal.png","product_mrp_price":"100","purchase_quantity":"5","total_price":"450","status":"0"}]
         */

        @SerializedName("total_products")
        private int totalProducts;
        @SerializedName("order_details")
        private OrderDetailsBean orderDetails;
        @SerializedName("status")
        private List<?> status;
        @SerializedName("products")
        private List<ProductsBean> products;

        public int getTotalProducts() {
            return totalProducts;
        }

        public void setTotalProducts(int totalProducts) {
            this.totalProducts = totalProducts;
        }

        public OrderDetailsBean getOrderDetails() {
            return orderDetails;
        }

        public void setOrderDetails(OrderDetailsBean orderDetails) {
            this.orderDetails = orderDetails;
        }

        public List<?> getStatus() {
            return status;
        }

        public void setStatus(List<?> status) {
            this.status = status;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class OrderDetailsBean {
            /**
             * reference_id : KM2019091604072927
             * order_confirmation_reference_id : KM2019091604072998
             * total_mrp_price : 450
             * discount : 0
             * shipping_charges : 0
             * final_price : 450
             * cancel_charges : 0
             * refund_price : 0
             * pincode : 500081
             * medium : ANDROID
             * order_status : Pending
             * status : 0
             * getaway_name : CcAvenue
             * ip_address : 157.48.97.171
             * expected_delivery : N/A
             * expected_delivery_slot : N/A
             * order_date : 16-09-2019 04:07 pm
             */

            @SerializedName("reference_id")
            private String referenceId;
            @SerializedName("order_confirmation_reference_id")
            private String orderConfirmationReferenceId;
            @SerializedName("total_mrp_price")
            private String totalMrpPrice;
            @SerializedName("discount")
            private String discount;
            @SerializedName("shipping_charges")
            private String shippingCharges;
            @SerializedName("final_price")
            private String finalPrice;
            @SerializedName("cancel_charges")
            private String cancelCharges;
            @SerializedName("refund_price")
            private String refundPrice;
            @SerializedName("pincode")
            private String pincode;
            @SerializedName("medium")
            private String medium;
            @SerializedName("order_status")
            private String orderStatus;
            @SerializedName("status")
            private String status;
            @SerializedName("getaway_name")
            private String getawayName;
            @SerializedName("ip_address")
            private String ipAddress;
            @SerializedName("expected_delivery")
            private String expectedDelivery;
            @SerializedName("expected_delivery_slot")
            private String expectedDeliverySlot;
            @SerializedName("order_date")
            private String orderDate;

            public String getReferenceId() {
                return referenceId;
            }

            public void setReferenceId(String referenceId) {
                this.referenceId = referenceId;
            }

            public String getOrderConfirmationReferenceId() {
                return orderConfirmationReferenceId;
            }

            public void setOrderConfirmationReferenceId(String orderConfirmationReferenceId) {
                this.orderConfirmationReferenceId = orderConfirmationReferenceId;
            }

            public String getTotalMrpPrice() {
                return totalMrpPrice;
            }

            public void setTotalMrpPrice(String totalMrpPrice) {
                this.totalMrpPrice = totalMrpPrice;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getShippingCharges() {
                return shippingCharges;
            }

            public void setShippingCharges(String shippingCharges) {
                this.shippingCharges = shippingCharges;
            }

            public String getFinalPrice() {
                return finalPrice;
            }

            public void setFinalPrice(String finalPrice) {
                this.finalPrice = finalPrice;
            }

            public String getCancelCharges() {
                return cancelCharges;
            }

            public void setCancelCharges(String cancelCharges) {
                this.cancelCharges = cancelCharges;
            }

            public String getRefundPrice() {
                return refundPrice;
            }

            public void setRefundPrice(String refundPrice) {
                this.refundPrice = refundPrice;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(String orderStatus) {
                this.orderStatus = orderStatus;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getGetawayName() {
                return getawayName;
            }

            public void setGetawayName(String getawayName) {
                this.getawayName = getawayName;
            }

            public String getIpAddress() {
                return ipAddress;
            }

            public void setIpAddress(String ipAddress) {
                this.ipAddress = ipAddress;
            }

            public String getExpectedDelivery() {
                return expectedDelivery;
            }

            public void setExpectedDelivery(String expectedDelivery) {
                this.expectedDelivery = expectedDelivery;
            }

            public String getExpectedDeliverySlot() {
                return expectedDeliverySlot;
            }

            public void setExpectedDeliverySlot(String expectedDeliverySlot) {
                this.expectedDeliverySlot = expectedDeliverySlot;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }
        }

        public static class ProductsBean {
            /**
             * id : 5
             * product_id : 5
             * product_name : Peanuts
             * images : 3542a-peanuts.png
             * product_mrp_price : 60
             * purchase_quantity : 5
             * total_price : 300
             * status : 0
             */

            @SerializedName("id")
            private String id;
            @SerializedName("product_id")
            private String productId;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("images")
            private String images;
            @SerializedName("product_mrp_price")
            private String productMrpPrice;
            @SerializedName("purchase_quantity")
            private String purchaseQuantity;
            @SerializedName("total_price")
            private String totalPrice;
            @SerializedName("status")
            private String status;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getProductMrpPrice() {
                return productMrpPrice;
            }

            public void setProductMrpPrice(String productMrpPrice) {
                this.productMrpPrice = productMrpPrice;
            }

            public String getPurchaseQuantity() {
                return purchaseQuantity;
            }

            public void setPurchaseQuantity(String purchaseQuantity) {
                this.purchaseQuantity = purchaseQuantity;
            }

            public String getTotalPrice() {
                return totalPrice;
            }

            public void setTotalPrice(String totalPrice) {
                this.totalPrice = totalPrice;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
