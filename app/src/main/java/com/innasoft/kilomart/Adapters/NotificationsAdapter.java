package com.innasoft.kilomart.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.NotificationActivity;
import com.innasoft.kilomart.R;
import com.innasoft.kilomart.Response.NotificationReadResponse;
import com.innasoft.kilomart.Response.NotificationsResponse;
import com.innasoft.kilomart.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.kilomart.Apis.RetrofitClient.IMAGE_BASE_URL;
import static com.innasoft.kilomart.Storage.Utilities.capitalize;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.Holder> {
    Context context;
    List<NotificationsResponse.DataBean> dataBeans;
    String read_status,send_id,userId,tokenValue;
    private PrefManager pref;
    AlertDialog alertDialog;

    public NotificationsAdapter(NotificationActivity notificationActivity, List<NotificationsResponse.DataBean> dataBeans) {
        this.context=notificationActivity;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public NotificationsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification_list, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsAdapter.Holder holder, final int i) {

        holder.notify_title_name.setText(capitalize(dataBeans.get(i).getTitle()));
        holder.notify_description.setText(dataBeans.get(i).getMessage());
        holder.notify_dateandtime.setText(dataBeans.get(i).getCreatedOn());

        Glide.with(context).load(IMAGE_BASE_URL+dataBeans.get(0).getImage()).error(R.drawable.default_loading).into(holder.imag_notification);

        read_status=dataBeans.get(i).getIsRead();
        if(read_status.equals("0"))
        {

            holder.linear_layout_row.setBackgroundColor(Color.parseColor("#d1f2e2"));
        }
        else
        {
            //nothing
        }

        pref = new PrefManager(context);
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");



        holder.linear_layout_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showCustomDialog(dataBeans.get(i).getMessage(),v,i);
            }
        });

    }

    private void showCustomDialog(String message, View view, final int i) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup =view.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(context).inflate(R.layout.my_customdialog, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        TextView txtmessage = (TextView) dialogView.findViewById(R.id.txtMessage);
        txtmessage.setText(message);


        Button buttonOk = (Button) dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                read_status=dataBeans.get(i).getIsRead();

                if(read_status.equals("0")) {

                    send_id=dataBeans.get(i).getId();

                    sendReadStatus(send_id);

                }
                else {

                    alertDialog.dismiss();
                }

            }
        });

        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void sendReadStatus(String send_id) {

        Call<NotificationReadResponse> call= RetrofitClient.getInstance().getApi().NotificationRead(tokenValue,userId,send_id);
        call.enqueue(new Callback<NotificationReadResponse>() {
            @Override
            public void onResponse(Call<NotificationReadResponse> call, Response<NotificationReadResponse> response) {
                if (response.isSuccessful());
                NotificationReadResponse baseResponse=response.body();
                if (baseResponse.getStatus().equals("10100")){

                    Intent intent = new Intent(context, NotificationActivity.class);
                    context.startActivity(intent);

                }
                else if (baseResponse.getStatus().equals("10200")){
                    Toast.makeText(context, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (baseResponse.getStatus().equals("10300")){
                    Toast.makeText(context, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (baseResponse.getStatus().equals("10400")){
                    Toast.makeText(context, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationReadResponse> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        ImageView imag_notification;
        TextView notify_title_name,notify_description,notify_dateandtime;
        LinearLayout linear_layout_row;

        public Holder(@NonNull View itemView) {
            super(itemView);

            notify_dateandtime=itemView.findViewById(R.id.notify_dateandtime);
            notify_description=itemView.findViewById(R.id.notify_description);
            notify_title_name=itemView.findViewById(R.id.notify_title_name);

            imag_notification=itemView.findViewById(R.id.imag_notification);

            linear_layout_row=itemView.findViewById(R.id.linear_layout_row);

        }
    }
}
