package com.innasoft.kilomart.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.innasoft.kilomart.OrdersListActivity;
import com.innasoft.kilomart.R;
import com.innasoft.kilomart.Response.MyOrdersResponse;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class OrderIdAdapter extends RecyclerView.Adapter<OrderIdAdapter.MyViewHolder> {
    private Context mContext;
    private List<MyOrdersResponse.DataBean.RecordDataBean> myOrdersListBeans;

    public OrderIdAdapter(Context mContext, List<MyOrdersResponse.DataBean.RecordDataBean> myOrdersListBeans) {
        this.mContext = mContext;
        this.myOrdersListBeans = myOrdersListBeans;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtOrderId, txtViewItems,txtId, txtInvoice, txtPaymentStatus,txtdd,txtmm,txtyy,
                txtIdTitle,txtOrderTitle,txtpaymentTitle;
        public CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtOrderId = (TextView) itemView.findViewById(R.id.txtOrderId);

            txtPaymentStatus=(TextView)itemView.findViewById(R.id.txtPaymentStatus);

            txtdd=(TextView)itemView.findViewById(R.id.txtdd);
            txtmm=(TextView)itemView.findViewById(R.id.txtmm);
            txtyy=(TextView)itemView.findViewById(R.id.txtyy);

            txtOrderTitle=(TextView)itemView.findViewById(R.id.txtOrderTitle);
            txtpaymentTitle=(TextView)itemView.findViewById(R.id.txtpaymentTitle);
            cardView=(CardView) itemView.findViewById(R.id.cardView);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.orderidlayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final MyOrdersResponse.DataBean.RecordDataBean orderListBean = myOrdersListBeans.get(position);

        setFadeAnimation(holder.itemView);

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();

        holder.cardView.setCardBackgroundColor(color1);

        holder.txtOrderTitle.setText("ORDER ID : ");
        holder.txtpaymentTitle.setText("Payment Type : ");
        holder.txtOrderId.setText( orderListBean.getOrderId());
        holder.txtPaymentStatus.setText(orderListBean.getPaymentStatus());


        Log.d("TIMEZONE","String To Date >> "+DateTimeUtils.formatDate(orderListBean.getOrderDate()));

        Date date = DateTimeUtils.formatDate(orderListBean.getOrderDate());
        String fullDate = DateTimeUtils.formatWithStyle(date, DateTimeStyle.MEDIUM); // June 13, 2017

        StringTokenizer stringTokenizer = new StringTokenizer(fullDate);
        String monthDate = stringTokenizer.nextToken(",");
        String year = stringTokenizer.nextToken(",");

        StringTokenizer monthdateToken = new StringTokenizer(monthDate);
        String mm = monthdateToken.nextToken(" ");
        String dd = monthdateToken.nextToken(" ");

        Log.e("TOKEN",""+mm+dd);

        holder.txtdd.setText(dd);
        holder.txtmm.setText(mm);
        holder.txtyy.setText(year);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String order_id=orderListBean.getId();

                Intent intent=new Intent(mContext,OrdersListActivity.class);
                intent.putExtra("Order_ID",order_id);
                intent.putExtra("CartStatus", false);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        // get Invoice
//        holder.txtInvoice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String url = "https://pickany24x7.com/Invoices/Invoice_" + orderListBean.getOrderId() + ".pdf";
//
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                mContext.startActivity(intent);
//            }
//        });
//
//        // view Items
//        holder.txtViewItems.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(mContext, OrdersListActivity.class);
//                intent.putExtra("orderId", orderListBean.getOrderId());
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                mContext.startActivity(intent);
//            }
//        });
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return myOrdersListBeans.size();
    }

    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}
