package com.innasoft.kilomart.Model;

public class OrderIdItem {

    private String orderId;
    private String orderDate;
    private String paymentId;
    private String orderstatus;
    private String paymentType;
    private String paymentStatus;
    private String transactionId;


    public OrderIdItem(String orderId, String orderDate, String paymentId, String orderstatus, String paymentType, String paymentStatus, String transactionId) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.paymentId = paymentId;
        this.orderstatus = orderstatus;
        this.paymentType = paymentType;
        this.paymentStatus = paymentStatus;
        this.transactionId = transactionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
