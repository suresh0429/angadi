package com.innasoft.kilomart.Model;

import java.util.List;

public class Product {
    private String id;
    private String product_id;
    private String product_name;
    private String selling_price;
    private String mrp_price;
    private int purchase_quantity;
    private String unit_id;
    private String unit_name;
    private String unit_value;
    private String brand_id;
    private String brand_name;
    private int net_amount;
    private int gross_amount;
    private String images;


    public Product(String id, String product_id, String product_name, String selling_price, String mrp_price, int purchase_quantity, String unit_id, String unit_name, String unit_value, String brand_id, String brand_name, int net_amount, int gross_amount, String images) {
        this.id = id;
        this.product_id = product_id;
        this.product_name = product_name;
        this.selling_price = selling_price;
        this.mrp_price = mrp_price;
        this.purchase_quantity = purchase_quantity;
        this.unit_id = unit_id;
        this.unit_name = unit_name;
        this.unit_value = unit_value;
        this.brand_id = brand_id;
        this.brand_name = brand_name;
        this.net_amount = net_amount;
        this.gross_amount = gross_amount;
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getSelling_price() {
        return selling_price;
    }

    public void setSelling_price(String selling_price) {
        this.selling_price = selling_price;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public int getPurchase_quantity() {
        return purchase_quantity;
    }

    public void setPurchase_quantity(int purchase_quantity) {
        this.purchase_quantity = purchase_quantity;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getUnit_value() {
        return unit_value;
    }

    public void setUnit_value(String unit_value) {
        this.unit_value = unit_value;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public int getNet_amount() {
        return net_amount;
    }

    public void setNet_amount(int net_amount) {
        this.net_amount = net_amount;
    }

    public int getGross_amount() {
        return gross_amount;
    }

    public void setGross_amount(int gross_amount) {
        this.gross_amount = gross_amount;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
