package com.innasoft.kilomart;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Response.VerifyOtpResponse;
import com.innasoft.kilomart.Storage.PrefManager;
import com.innasoft.kilomart.Storage.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOtpActivity extends Activity {
    private static final String PREFS_LOCATION = "LOCATION_PREF";
    public static String TAG ="VerifyOtpActivity";

        @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.logo_img)
    ImageView logoImg;
    @BindView(R.id.otp_txt)
    TextView otpTxt;
    @BindView(R.id.pinview)
    Pinview pinview;
    @BindView(R.id.verify_btn)
    Button verifyBtn;
    @BindView(R.id.txtresend)
    TextView txtresend;
    String mno,otp,deviceID;
    PrefManager session;

    String fcmToken;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        session=new PrefManager(VerifyOtpActivity.this);

        deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        mno=getIntent().getStringExtra("MobileNumber");

        // fcm Token
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(VerifyOtpActivity.this,new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.e("TOKEN",""+fcmToken);
            }
        });

    }

    @OnClick({ R.id.otp_txt, R.id.pinview, R.id.verify_btn, R.id.txtresend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.verify_btn:

                otp=pinview.getValue().toString().trim();
                progressBar.setVisibility(View.VISIBLE);
                Call<VerifyOtpResponse> call= RetrofitClient.getInstance().getApi().userOtpRequest(mno,otp,deviceID);
                call.enqueue(new Callback<VerifyOtpResponse>() {
                    @Override
                    public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {

                        if (response.isSuccessful());
                        VerifyOtpResponse verifyOtpResponse=response.body();

                        if (verifyOtpResponse.getStatus().equals("10100")){

                            progressBar.setVisibility(View.GONE);

                            session.createLogin(verifyOtpResponse.getData().getUser_id(),
                                    verifyOtpResponse.getData().getUser_name(),
                                    verifyOtpResponse.getData().getEmail(),
                                    verifyOtpResponse.getData().getMobile(),
                                    null,deviceID,
                                    verifyOtpResponse.getData().getJwt(),verifyOtpResponse.getData().getGender());

                            Intent intent = new Intent(VerifyOtpActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);


                            // location preferences
                            setDefaults();

                            updateFcmToken(verifyOtpResponse.getData().getJwt(),verifyOtpResponse.getData().getUser_id());

                            Toast.makeText(getApplicationContext(),"You have been logged in successfully.",Toast.LENGTH_SHORT).show();
                        }

                        else if (verifyOtpResponse.getStatus().equals("10200")){
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(VerifyOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (verifyOtpResponse.getStatus().equals("10300")){
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(VerifyOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (verifyOtpResponse.getStatus().equals("10400")){
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(VerifyOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(VerifyOtpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

                break;
            case R.id.txtresend:
                progressBar.setVisibility(View.VISIBLE);
                Call<BaseResponse> call1=RetrofitClient.getInstance().getApi().ResendOtpRequest(mno);
                call1.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful());
                        BaseResponse resendOtpResponse=response.body();

                        if (resendOtpResponse.getStatus().equals("10100")){
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(VerifyOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (resendOtpResponse.getStatus().equals("10200")){
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(VerifyOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (resendOtpResponse.getStatus().equals("10300")){
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(VerifyOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        else if (resendOtpResponse.getStatus().equals("10400")){
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(VerifyOtpActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(VerifyOtpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


                    }
                });
                break;
        }
    }


    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);
        editor.putString(Utilities.KEY_AREA,"");
        editor.putString(Utilities.KEY_PINCODE,"");
        editor.putString(Utilities.KEY_SHIPPINGCHARGES,"");
        editor.apply();
    }
    // update Fcm Token
    private void updateFcmToken(String jwt, String user_id) {

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateFcmTocken(jwt,user_id,fcmToken);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {


                progressBar.setVisibility(View.GONE);


                BaseResponse loginResponse = response.body();


                if (loginResponse.getStatus().equals("10100")) {

                   // Toast.makeText(VerifyOtpActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());

                }else if (loginResponse.getStatus().equals("10200")){
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());
                }
                else if (loginResponse.getStatus().equals("10300")){
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());
                }
                else if (loginResponse.getStatus().equals("10400")){
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());
                }



            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

    }
}
