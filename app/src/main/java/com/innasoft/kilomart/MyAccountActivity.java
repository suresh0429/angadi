package com.innasoft.kilomart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyAccountActivity extends AppCompatActivity {


    @BindView(R.id.txtAccName)
    TextView txtAccName;
    @BindView(R.id.txtAccMobile)
    TextView txtAccMobile;
    @BindView(R.id.txtMail)
    TextView txtMail;
    @BindView(R.id.ChangePassword)
    CardView ChangePassword;
    @BindView(R.id.myAddressCard)
    CardView myAddressCard;
    @BindView(R.id.myOrdersCard)
    CardView myOrdersCard;
    @BindView(R.id.myWhishListCard)
    CardView myWhishListCard;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.parentLayout)
    NestedScrollView parentLayout;
    @BindView(R.id.editProfile)
    ImageView editProfile;
    @BindView(R.id.myNotificationsCard)
    CardView myNotificationsCard;

    private PrefManager pref;
    String userId;
    boolean checkoutStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Account");



        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }


        AppController app = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());

        // Method to manually check connection status

        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String mobile = profile.get("mobile");
        String email = profile.get("email");
        String imagePic = profile.get("profilepic");


        txtAccName.setText(username);
        txtAccMobile.setText(mobile);
        txtMail.setText(email);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.ChangePassword, R.id.myAddressCard, R.id.myOrdersCard, R.id.myWhishListCard,R.id.myNotificationsCard, R.id.editProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ChangePassword:
                Intent editMyAcc = new Intent(MyAccountActivity.this, ChangePasswordActivity.class);
                editMyAcc.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(editMyAcc);

                break;
            case R.id.myAddressCard:
                Intent address = new Intent(MyAccountActivity.this, AddressListActivity.class);
                address.putExtra("Checkout", false);
                address.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(address);

                break;
            case R.id.myOrdersCard:
                Intent orders = new Intent(MyAccountActivity.this, OrderIdActvity.class);
                orders.putExtra("Checkout", false);
                orders.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(orders);

                break;
            case R.id.myWhishListCard:
                Intent wishlist = new Intent(MyAccountActivity.this, WishListActivity.class);
                wishlist.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(wishlist);

                break;
            case R.id.editProfile:
                Intent vechils = new Intent(MyAccountActivity.this, UpdateProfileActivity.class);
                vechils.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(vechils);

                break;
            case R.id.myNotificationsCard:
                Intent service = new Intent(MyAccountActivity.this, NotificationActivity.class);
                service.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(service);

                break;
        }
    }
}
